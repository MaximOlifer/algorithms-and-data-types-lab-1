#pragma once
#include <iostream>

using namespace std;

class El {
	char e;
	El *next;
	static const int maxmup = 300;
	static El mem[maxmup]; // ��������� ������ ��� ��������� ������
	static int mup, mup0;
public:
	El(): e('!'), next(nullptr) {}
	El(char e, El *n = nullptr) : e(e), next(n) {
		// std::cout << "+" << e;
	}
	~El() {
		if (this) {
			if (next) { delete next;}
			//std::cout << "-" << e;
			e = '*';
		}
	//	else cout << "<�����!>";
	}
	static void* operator new(size_t) {
		return (mup < maxmup ? &mem[mup++] : nullptr);
	}
	static void operator delete(void *, size_t) {}
	static void mark() { mup0 = mup; } // ����������� ��������� ������
	static void release() { mup = mup0;  } // �������� �� ��������������
	static void clear() { mup = 0; } // �������� ������ ���������
	friend static void memOut();
	friend class Set_List;

	void Show() { for (El* p = this; p; p=p->next) cout << p->e; }
};

El El::mem[El::maxmup]; // "��������� ������"
int El::mup = 0, El::mup0 = 0;


void memOut() { // ���� ��������������� ����������� "��������� ������"
	//std::cout << "\n������ ��������� ������� (����� - " << std::dec << El::mup << ")\n";
	for (int i = 0; i < El::mup; ++i) cout << El::mem[i].e;
}


class Set_List
{
private:
	static const int N; // �������� ����������
	static int num; // ���������� ����� ���������
	int n; // �������� ���������
	char S; // ���
	El *A; // ������ ���������
public:
	Set_List(); // ������ ���������
	Set_List(char); // ��������� ������������ �������� (�������� ������������)
	Set_List(const Set_List &);
	Set_List(char* str, char s, int N);
	Set_List(Set_List &&);
	~Set_List() {
		// std::cout << "�������" << S << "(" << std::dec << n << ") = ["; A->Show(); cout << "]" << endl;
		delete A;
	}
	void Show();
	int power() { return n; }
	void swap(Set_List & other) { 
		std::swap(S, other.S); 
		std::swap(n, other.n); 
		std::swap(A, other.A);
	}
	
	Set_List& operator &= (const Set_List&);
	Set_List operator & (const Set_List&) const;
	Set_List operator = (const Set_List &B);
	Set_List& operator=(Set_List &&B);
};


Set_List::Set_List() : n(0), S('A'), A(nullptr) {
	//std::cout << "-> �������" << S << "(" << n << ") = ["; A->Show(); cout << "] \n";
}


Set_List::Set_List(char) : S('A'), n(0) {
	A = nullptr;
	for (int i = 0; i < N; i++)
		if (rand() % 2)
			A = new El(i + 'A', A), ++n;
	// std::cout << "-> ������� " << S << "(" << n << ") = ["; A->Show(); cout << "] \n";
}


Set_List::Set_List(const Set_List & B) : n(B.n), S(B.S), A(nullptr) {
	for (El * p = B.A; p; p = p->next) A = new El(p->e, A);
	// std::cout << "-> ������� " << S << "(" << n << ") = ["; A->Show(); cout << "] �� " << B.S << endl;
}


Set_List::Set_List(char* str, char s, int N) : n(N), S(s), A(nullptr) {
	for (auto i = 0; i < N; i++) A = new El(str[i], A);
	// std::cout << "-> ������� " << S << "(" << n << ") = [";  A->Show();  cout << "]" << endl;
}


Set_List::Set_List(Set_List && B) : n(B.n), S(B.S), A(B.A) {
	B.A = nullptr;
	// std::cout << "-> ������� " << S << "(" << n << ") = ["; A->Show(); cout << "] �� " << B.S << endl;
}


Set_List & Set_List::operator &= (const Set_List&B) {
	Set_List C;
	for (El * i = A; i; i = i->next) {
		for (El * j = B.A; j; j=j->next)
			if (i->e == j->e)
				C.A = new El(i->e, C.A), ++C.n;
	}
	swap(C);
	this->S = C.S;
	// std::cout << "-> �������� " << S << "(" << n << ") = ["; A->Show(); cout << "] = " << C.S << "&" << B.S << endl;
	return *this;
}
Set_List Set_List::operator & (const Set_List& B) const {
	Set_List C(*this);
	// cout << "����������" << C.S << "&" << B.S << endl;
	return C &= B;
}

Set_List Set_List::operator= (const Set_List & B) {
	if (this != &B) {
		delete A;
		A = nullptr;
		n = 0;
		for (El *p = B.A; p; p = p->next)
			A = new El(p->e, A), ++n;
		S = 'R';
	}
	return S;
}

Set_List& Set_List::operator=(Set_List &&B) {
	swap(B);
	S = 'R';
	delete B.A;
	B.A = nullptr;
	return *this;
}

void Set_List::Show() {
	cout << S  << " = [";
	for (El *p = A; p; p = p->next) 
		cout << p->e;
	cout << "]\n";
}
