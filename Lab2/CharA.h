#pragma once
#include <iostream>
#include <string.h>
#include "pch.h"
using namespace std;

class CharA 
{
private:
	static const int N = 10;
	int n;
	char S, *A;

public:
	CharA operator & (const CharA&) const;
	CharA& operator &= (const CharA&);
	void Show() { cout << S << " = [" << A << "]" << endl; }
	operator int() { return n; }
	explicit CharA(char);
	CharA();
	CharA(const CharA&);
	CharA(char, const char* a);
	CharA & operator = (const CharA&);
	~CharA() { 
		delete[] A; 
		// cout << "������� " << S << endl; 
	}

	Set_List toList() { return Set_List(A, S, n); }
	Bits toBits() { return Bits(A, S, n); }
	Word toWord() { return Word(A, S, n); }
};

CharA::CharA() : n(0), S('A'), A(new char[N + 1])
{
	A[0] = '\0';
}

CharA::CharA(const CharA & B) : S(B.S), n(B.n), A(new char[N + 1])
{
	char *s(B.A), *t(A);
	while (*t++ = *s++);
	// cout << "������� " << S << "(" << n << ") = " << A << endl;
}

CharA::CharA(char c, const char* a) : S(c), A(new char[N + 1])
{
	strcpy_s(A, N+1, a);
	n = strlen(a);
	// cout << "������� " << S << "(" << n << ") = " << A << endl;
}

CharA& CharA :: operator= (const CharA& B)
{
	if (this != &B)
	{
		char *s(B.A), *t(A);
		n = B.n;
		S = 'R';
		while (*t++ = *s++);
	}
	return *this;
}
CharA& CharA :: operator &= (const CharA& B)
{
	CharA C(*this);
	n = 0;
	for (int i = 0; i < C.n; ++i) {
		for (int j = 0; j < B.n; j++)
			if (C.A[i] == B.A[j]) A[n++] = C.A[i];
	}
	A[n] = '\0';
	return *this;
	// std::cout << "-> �������� " << S << "(" << n << ") = [" << A << "] = " << C.S << "&" << B.S << endl;
}

CharA CharA :: operator & (const CharA& B) const
{
	CharA C(*this);
	// cout << "����������" << S << "&" << B.S << endl;
	return(C &= B);
}

CharA::CharA(char s) : S(s), n(0), A(new char[N + 1])
{
	for (int i = 0; i < N; i++)
		if (rand() % 2) A[n++] = i + '0';
	A[n] = 0;
	// cout << "������� " << S << "(" << n << ") = " << A << endl;
}
