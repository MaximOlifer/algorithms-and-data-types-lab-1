#pragma once
#include <iostream>
#include "pch.h"
using namespace std;

class Bits {
private:

	static const int N = 10;
	char S;
	bool *A;

public:
	Bits operator & (const Bits&) const;	
	Bits& operator &= (const Bits&);
	void Show();
	operator int() { return N; }
	explicit Bits(char);
	explicit Bits(char*, char, int);
	Bits();	
	Bits(const Bits&);
	Bits & operator = (const Bits&);
	~Bits() { 
		delete[] A; 
		A = nullptr; 
		// cout << "������� " << S << endl;
	}
};


Bits::Bits() : S('0'), A(new bool[N])
{
	for (auto i = 0; i < N; i++)
		A[i] = 0;
}

Bits::Bits(const Bits & B) : S(B.S), A(new bool[N])
{
	for (auto i = 0; i < N; i++)
		A[i] = B.A[i];
	// cout << "������� " << S << endl;
}

Bits & Bits :: operator = (const Bits& B)
{
	if (this != &B)
	{
		S = 'R';
		for (auto i = 0; i < N; i++)
			A[i] = B.A[i];
	}
	return *this;
}
Bits& Bits :: operator &= (const Bits& B)
{
	Bits C(*this);
	for (int i = 0; i < C.N; ++i) {
		A[i] = C.A[i] && B.A[i];
	}
	return *this;
}

Bits Bits :: operator & (const Bits& B) const
{
	Bits C(*this);
	// cout << "���������� " << S << "&" << B.S << endl;
	return(C &= B);
}

Bits::Bits(char s) : S(s), A(new bool[N])
{
	int q;
	for (int i = 0; i < N; i++) {
		q = rand() % 2;
		A[i] = q;
	}
	cout << endl << S << " = [";
	for (auto i = 0; i < N; i++)
		cout << A[i]<<" ";
	cout << "]" << endl;
}

Bits::Bits(char* a, char s, int n) : S(s), A(new bool[N])
{
	for (int i = 0; i < N; i++)
		A[i] = 0;

	for (int i = 0; i < n; i++) {
		A[char_to_digit(a[i])] = 1;
	}
	// cout << "������� " << S << endl;
}

void Bits:: Show()
{
	cout << S << " = [";
	for (auto i = 0; i < N; i++)
		cout << A[i] << " ";
	cout << "]" << endl;
}