// Лабораторная работа 2, вариант 25
// Copyleft by Олифер, Петров, г. 8308


#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

#include "pch.h"

const int test_count = 100000;
const int Set_List::N = 10;
int Set_List::num = 0;

template<class Set>
void test(Set A, Set B, Set C, Set D) 
{
	unsigned long start_t,
					end_t;
	Set R;

	A.Show();
	B.Show();
	C.Show();
	D.Show();

	R = A & B & C & D;
	R.Show();

	start_t = clock();
	for (int l = 0; l < test_count-1; l++) {
		R = A & B & C & D;
	}
	
	end_t = clock();

	cout << "time: " << end_t - start_t << endl;
}

int main() 
{
	srand(time(nullptr));
	CharA A('A', "715420869"), B('B', "219706384"), C('C', "389157064"), D('D', "597632104");
	Bits Ab = A.toBits(), 
		Bb = B.toBits(), 
		Cb = C.toBits(), 
		Db = D.toBits();
	Word Aw = A.toWord(),
		Bw = B.toWord(),
		Cw = C.toWord(),
		Dw = D.toWord();
	Set_List Al = A.toList(),
			Bl = B.toList(),
			Cl = C.toList(),
			Dl = D.toList();

	cout << "CharA" << endl;
	test<CharA>(A, B, C, D);

	cout << endl << "Bits" << endl;
	test<Bits>(Ab, Bb, Cb, Db);

	cout << endl << "Word" << endl;
	test<Word>(Aw, Bw, Cw, Dw);

	cout << endl << "List" << endl;
	test<Set_List>(Al, Bl, Cl, Dl);

	system("pause");
	return 0;
}
