#pragma once
// Converts char ('0'-'9') to digit (0-9)
// Returns -1 in case of wrong input
int char_to_digit(char c) {
	if ('0' <= c && c <= '9')
		return c - '0';
	else
		return -1;
}
