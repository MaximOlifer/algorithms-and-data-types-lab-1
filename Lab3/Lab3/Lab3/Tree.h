#pragma once
#include <iostream>

using namespace std;

//���� ������
class Node {
	char d;
	Node* lft;
	Node* rgt;
public:
	Node() : lft(nullptr), rgt(nullptr) {};
	~Node() {
		if (rgt) delete rgt;
		if (lft) delete lft;
	}
	friend class Tree;
};

//������ � �����
class Tree
{
	Node* root;
	char num, maxnum;
	int maxrow, offset;
	char** SCREEN;
	void clrscr();
	Node* MakeNode(int depth);
	void OutNodes(Node* v, int r, int c);
	Tree(const Tree &);
	Tree operator = (const Tree &) const {};
public:
	Tree(char num, char maxnum, int maxrow);
	~Tree() {
		for (int i = 0; i < maxrow; ++i)
			delete[] SCREEN[i];
		delete[] SCREEN;
		delete root;
	}
	void MakeTree() {
		root = MakeNode(0);
	}
	bool exist() {
		return root != nullptr;
	}
	int DFS() {};
	int BFS();
	void OutTree();
};

Tree::Tree(char nm, char mnm, int mxr) : num(nm), maxnum(mnm), maxrow(mxr), offset(40), root(nullptr), SCREEN(new char *[maxrow])
{
	for (int i = 0; i < maxrow; ++i) SCREEN[i] = new char[80];
}


Node * Tree::MakeNode(int depth)
{
	Node * v = nullptr;
	int Y = (depth < rand() % 8 + 1) && (num <= 'z');
	/* ������� � ������������ ������ �������� ����
	cout << "Node ("<< num << ',' << depth << ")1/0: ";
	char c;
	cin >> c; */
	if (Y) {
		// �������� ����, ���� Y = 1, ������� ������ � � �������� �������
		v = new Node;				 //|
		v->lft = MakeNode(depth + 1);//V
		v->rgt = MakeNode(depth + 1);//V
		v->d = num++;		// <-<-<-<-'
	}
	return v;
}

void Tree::OutTree()
{
	clrscr();
	OutNodes(root, 1, offset);
	for (int i = 0; i < maxrow; ++i)
	{
		SCREEN[i][79] = 0;
		cout << '\n' << SCREEN[i];
	}
	cout << '\n';
}

void Tree::clrscr()
{
	for (int i = 0; i < maxrow; ++i)
		memset(SCREEN[i], '.', 80);
}

void Tree::OutNodes(Node * v, int r, int c)
{
	if (r && c && (c < 80)) SCREEN[r - 1][c - 1] = v->d; // ����� �����
	if (r < maxrow) {
		if (v->lft) OutNodes(v->lft, r + 1, c - (offset >> r)); //����� ���
		if (v->rgt) OutNodes(v->rgt, r + 1, c + (offset >> r)); //������ ���
	}
}

template <class Item> class QUEUE
{
	Item * Q; int head, tail, n, N;
public:
	QUEUE(int maxQ) : head(0), tail(0), n(0), N(maxQ), Q(new Item[maxQ + 1]) { }
	bool empty() const
	{
		return (head % N) == tail;
	}
	int size() const
	{
		return n;
	}
	void put(Item item)
	{
		Q[tail++] = item;
		tail %= N;
		++n;
	}	
	Item front() const
	{
		return Q[head % N];
	}
	void pop()
	{
		head++;
		head %= N;
		--n;
	}
};

int Tree::BFS()
{
	const int MaxQ = 20; //������������ ������ �������
	int count = 0;
	QUEUE < Node * > Q(MaxQ);  //�������� ������� ���������� �� ����
	Q.put (root); // QUEUE <- root ��������� � ������� ������ ������
	while (!Q.empty()) //���� ������� �� �����
	{
		Node * v = Q.front();
		Q.pop();// ����� �� �������,
		cout << v->d << '_';
		if((v->lft && !v->rgt && !v->lft->lft && !v->lft->rgt) 
			|| (!v->lft && v->rgt && !v->rgt->lft && !v->rgt->rgt)
			|| (v->lft && v->rgt && !v->lft->lft && !v->lft->rgt && !v->rgt->lft && !v->rgt->rgt)
			|| (v->lft && !v->rgt && v->lft->lft && !v->lft->rgt && !v->lft->lft->lft && !v->lft->lft->rgt)
			|| (v->lft && !v->rgt && !v->lft->lft && v->lft->rgt && !v->lft->rgt->lft && !v->lft->rgt->rgt)
			|| (!v->lft && v->rgt && v->rgt->lft && !v->rgt->rgt && !v->rgt->lft->lft && !v->rgt->lft->rgt)
			|| (!v->lft && v->rgt && !v->rgt->lft && v->rgt->rgt && !v->rgt->rgt->lft && !v->rgt->rgt->rgt)
			|| (!v->lft && !v->rgt))
			++count; // ������ ���, ���� �����
		if (v->lft) Q.put(v->lft); // QUEUE <- (����� ���)
		if (v->rgt) Q.put(v->rgt); // QUEUE <- (������ ���)
	}
	return count;
}
