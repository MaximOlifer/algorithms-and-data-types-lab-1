#include <iostream>
#include <time.h>

using namespace std;

#include "Tree.h"

void main()
{
	int n = 0;
	Tree Tr('a', 'z', 8);
	srand(time(nullptr));
	setlocale(LC_ALL, "Russian");
	Tr.MakeTree();
	if (Tr.exist()) {
		Tr.OutTree();
		cout << '\n' << "����� � ������: ";
		n = Tr.BFS();
		cout << " ���������� ����� = " << n;
	}
	else cout << "������ �����!";
	cout << '\n' << "=== ����� ===";
	getchar();
}
