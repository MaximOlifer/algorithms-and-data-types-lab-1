// Лабораторная работа 1, вариант 25
// Copyleft by Олифер, Петров, г. 8308

#include <iostream>
#include <list>
#include <time.h>

using namespace std;
const int U = 10;
const int test_count = 100000;

struct Set {
	char el;
	Set * next;
	~Set() { delete next; }
};


Set *convert_string_to_list(char *string);
char *convert_list_to_string(Set *L);
bool *convert_string_to_bool_array(char *string);
char *convert_bool_array_to_string(bool *bool_array);
short convert_string_to_word(char *string);
char *convert_word_to_string(short word);

char* alg_array(char *A, char *B, char *C, char *D);
bool *alg_boolean_array(bool *A, bool *B, bool *C, bool *D);
short alg_machine_word(short A, short B, short C, short D);
Set *alg_list(Set *A, Set *B, Set *C, Set *D);

int main() {
	char A[U + 1] = {};
	char B[U + 1] = {};
	char C[U + 1] = {};
	char D[U + 1] = {};

	char *E;

	int menu;

	do {
		cout << "A = ";
		cin >> A;
		cout << "B = ";
		cin >> B;
		cout << "C = ";
		cin >> C;
		cout << "D = ";
		cin >> D;
		cout << endl;

		// Char array
		cout << "Array:" << endl;
		E = alg_array(A, B, C, D);
		cout << "E = " << E << endl << endl;
		delete E;

		// Set-list
		cout << "List:" << endl;
		Set *E_list = alg_list(convert_string_to_list(A),
			convert_string_to_list(B),
			convert_string_to_list(C),
			convert_string_to_list(D));
		E = convert_list_to_string(E_list);
		delete E_list;
		cout << "E = " << E << endl;
		delete E;
		
		

		// Boolean array
		cout << "Bool array:" << endl;
		bool *E_bool_array = alg_boolean_array(
			convert_string_to_bool_array(A),
			convert_string_to_bool_array(B),
			convert_string_to_bool_array(C),
			convert_string_to_bool_array(D)
		);
		E = convert_bool_array_to_string(E_bool_array);
		cout << "E = " << E << endl;
			
		delete E_bool_array;
		delete E;

		// Machine word
		cout << "Machine word:" << endl;
		short E_machine_word = alg_machine_word(
			convert_string_to_word(A),
			convert_string_to_word(B),
			convert_string_to_word(C),
			convert_string_to_word(D)
		);
		E = convert_word_to_string(E_machine_word);
		cout << "E = " << E << endl;
		delete E;

		cout << "restart? (1/0)";
		cin >> menu;
	} while (menu);

	return 0;
}

/// CONVERTION ALGORITHMS
// Converts char ('0'-'9') to digit (0-9)
// Returns -1 in case of wrong input
int char_to_digit(char c) {
	if ('0' <= c && c <= '9')
		return c - '0';
	else
		return -1;
}

// Converts digit (0-9) to char
// Returns '0' in case of wrong input
char digit_to_char(int d) {
	if (0 <= d && d <= 9)
		return d + '0';
	else
		return '0';
}

Set *convert_string_to_list(char *string) {
	Set *List_head = nullptr;
	Set *pser = nullptr;
	Set *pser1 = nullptr;
	int lenght = strlen(string),
		i;

	if (lenght != 0)
	{
		List_head = new Set;
		if (List_head)
		{
			List_head->el = string[0];
			List_head->next = nullptr;
			pser = List_head;
			for (i = 1; string[i] != '\0'; i++) {
				pser1 = new Set;
				if (pser1) {
					pser->next = pser1;
					pser1->el = string[i];
					pser1->next = nullptr;
					pser = pser1;
					pser1 = pser1->next;
				}
			}
		}
	}
	return List_head;
}

char *convert_list_to_string(Set *List_head) {
	char *string = new char[U + 1];
	Set* pser = List_head;
	int i = 0;

	if (List_head)
	{
		do
		{
			string[i] = pser->el;
			i++;
			pser = pser->next;
		} while (pser != nullptr);
		string[i] = '\0';
	}
	else
		string[0] = '\0';

	return string;
}

// Converts string set to boolean array
// Data represented as: bool_array[i] = {1 if (set contains i) else 0}
bool *convert_string_to_bool_array(char *string) {
	bool *bool_array = new bool[U];
	for (int i = 0; i < U; i++)
		bool_array[i] = 0;

	for (int i = 0; string[i] != '\0'; i++) {
		bool_array[char_to_digit(string[i])] = 1;
	}

	return bool_array;
}

// Converts string set to boolean array
// Data should be represented as: bool_array[i] = {1 if (set contains i) else 0}
char *convert_bool_array_to_string(bool *bool_array) {
	char *string = new char[U + 1];
	int k = 0;
	
	for (int i = 0; i < U; i++) {
		if (bool_array[i] == 1)
			string[k++] = digit_to_char(i);
	}

	string[k++] = '\0';
	return string;
}

// Converts string to machine word
// Data represented as: ((word >> i) & 1) = {1 if (set contains i) else 0}
short convert_string_to_word(char *string) {
	short word = 0;
	for (int i = 0; string[i] != '\0'; i++)
		word |= (1 << char_to_digit(string[i]));
	return word;
}

// Converts machine word to string
// Data represented as: ((word >> i) & 1) = {1 if (set contains i) else 0}
char *convert_word_to_string(short word) {
	char *string = new char[U + 1];

	int k = 0;
	for (int i = 0; i < U; i++) {
		if (((word >> i) & 1) == 1)
			string[k++] = digit_to_char(i);
	}
	string[k++] = '\0';
	return string;
}

/// MAIN ALGORITHMS
char *alg_array(char *A, char *B, char *C, char *D) {
	char *E = new char[U + 1];

	unsigned long start_t,
					end_t;

	start_t = clock();
	for (int l = 0; l < test_count; l++) {
		int k = 0;
		for (int i = 0; A[i] != '\0'; i++) {
			for (int j = 0; B[j] != '\0'; j++) {
				if (A[i] == B[j])
					for (int m = 0; C[m] != '\0'; m++) {
						if (B[j] == C[m])
							for (int n = 0; D[n] != '\0'; n++) {
								if (C[m] == D[n])
									E[k++] = A[i];
							}
					}
			}
		}
		E[k++] = '\0';
	}
	end_t = clock();
	cout << "time: " << end_t - start_t << "ms" << endl;

	return E;
}

bool *alg_boolean_array(bool *A, bool *B, bool *C, bool *D) {
	bool *E_bool_array = new bool[U];
	unsigned long start_t,
				end_t;

	start_t = clock();
	for (int l = 0; l < test_count; l++) {
		for (int i = 0; i < U; i++)
			E_bool_array[i] = A[i] & B[i] & C[i] & D[i];
	}
	end_t = clock();
	cout << "time: " << end_t - start_t << "ms" << endl;

	delete A;
	delete B;
	delete C;
	delete D;
	return E_bool_array;
}


short alg_machine_word(short A, short B, short C, short D) {
	short res;
	unsigned long start_t,
				end_t;

	start_t = clock();
	for (int l = 0; l < test_count; l++) {
		res = A & B & C & D;
	}
	end_t = clock();
	cout << "time: " << end_t - start_t << "ms" << endl;
	return res;
}

Set *alg_list(Set *A, Set *B, Set *C, Set *D) {
	Set *E = nullptr;
	Set *e = nullptr;
	Set *prev = nullptr;

	unsigned long start_t,
					end_t;

	start_t = clock();
	for (int l = 0; l < test_count; l++) {
		E = nullptr;
		for (Set *a = A; a != nullptr; a = a->next) {
			for (Set *b = B; b != nullptr; b = b->next) {
				if (a->el == b->el) {
					for (Set *c = C; c != nullptr; c = c->next) {
						if (b->el == c->el) {
							for (Set *d = D; d != nullptr; d = d->next) {
								if (c->el == d->el) {
									e = new Set;
									e->el = a->el;
									e->next = nullptr;
									if (E == nullptr) {
										E = e;
									}
									else {
										prev->next = e;
									}
									prev = e;
								}
							}
						}
					}
				}
			}
		}
		if (l != test_count - 1)
			delete E;
	}
	end_t = clock();
	cout << "time: " << end_t - start_t << "ms" << endl;

	delete A;
	delete B;
	delete C;
	delete D;

	return E;
}
